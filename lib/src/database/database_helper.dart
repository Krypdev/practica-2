import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:practica_2/src/models/userdao.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _nameDB = 'PATM2020';
  static final _versionDB = 1;

  static Database _database;

  Future<Database> get getDatabase async {
    if(_database != null) return _database;
    
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory folder = await getApplicationDocumentsDirectory();
    String routeDB = join(folder.path, _nameDB);
    return await openDatabase(
      routeDB, 
      version: _versionDB,
      onCreate: _createTables
    );
  }

  _createTables(Database db, int version) async {
    await db.execute("CREATE TABLE tbl_perfil(id INTEGER PRIMARY KEY, username VARCHAR(30), password VARCHAR(30), nameUser VARCHAR(30), lastPUser VARCHAR(30), lastMUser VARCHAR(30), phone CHAR(12), email VARCHAR(50), photo VARCHAR(255))");
  }

  Future<int> insert(Map<String, dynamic> row, String table) async {
    var dbClient = await getDatabase;
    return await dbClient.insert(table, row);
  }

  Future<int> update(Map<String, dynamic> row, String table) async {
    var dbClient = await getDatabase;
    return await dbClient.update(table, row, where: 'id = ?', whereArgs: [row['id']]);
  }

  Future<int> delete(int id, String table) async {
    var dbClient = await getDatabase;
    return await dbClient.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  Future<UserDAO> getUser(String email) async {
    var dbClient = await getDatabase;
    var result = await dbClient.query('tbl_perfil', where: 'email = ?', whereArgs: [email]);
    var list = (result).map((element) => UserDAO.fromJSON(element)).toList();
    return list.length > 0 ? list[0] : null;
  }
}