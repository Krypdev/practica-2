import 'package:flutter/material.dart';
import 'package:practica_2/src/assets/configuration.dart';
import 'package:practica_2/src/network/api_login.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

    Configuration conf = Configuration();
    ApiLogin httpLogin = ApiLogin();

    bool isValidating = false;

  @override
  Widget build(BuildContext context) {

    TextEditingController txtuser = TextEditingController();
    TextEditingController txtpwa = TextEditingController();

    final logo = CircleAvatar(
      child: Image.network('http://itcelaya.edu.mx/jornadabioquimica/wp-content/uploads/2019/07/cropped-LOGO-ITC-843x1024.png', height: 150,),
      radius: 100,
      backgroundColor: Colors.orangeAccent,
    );

    final txtEmail = TextFormField(
      controller: txtuser,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: 'Introduce el email',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtPwd = TextFormField(
      controller: txtpwa,
      keyboardType: TextInputType.text,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Introduce la contraseña',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      ),
    );

    final checkSession = CheckboxListTile(
      title: const Text('Mantener sesión iniciada'),
      value: timeDilation != 1.0,
      onChanged: (bool value) {
        setState(() {
          timeDilation = value ? 2.0 : 1.0;
          Configuration conf = new Configuration();
          conf.setActiveSession(value);
        });
      },
      controlAffinity: ListTileControlAffinity.leading,
      secondary: const Icon(Icons.admin_panel_settings_rounded),
    );

    final btnLogin = RaisedButton(
      color: Colors.orangeAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text('Validar usuario', style: TextStyle(color: Colors.white),),
      onPressed: () async {

        setState(() {
          isValidating = true;
        });

        //UserDAO objUser = UserDAO(email: txtuser.text, password: txtpwa.text);
        if (txtuser.text.length > 0) {
          await conf.setToken(txtuser.text);
          await conf.setEmail(txtuser.text);
          Navigator.pop(context);
          Navigator.pushNamed(context, "/dashboard");
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context){
              return AlertDialog(
                title: Text('Error'),
                content: Text('Tecle el email'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () => Navigator.of(context).pop(),
                  )
                ],
              );
            }
          );
        }
        
        /* final token = await httpLogin.validateUser(objUser);
        
        if (token != null) {
          if(checkSession.value) await conf.setToken(token.accessToken);
          await conf.setEmail(objUser.email);
          Navigator.pop(context);
          Navigator.pushNamed(context, "/dashboard");
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context){
              return AlertDialog(
                title: Text('Error'),
                content: Text('The credentials are incorrect'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () => Navigator.of(context).pop(),
                  )
                ],
              );
            }
          );
        } */
      }
    );

    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/01.jpg'),
              fit: BoxFit.fitHeight,
            )
          ),
        ),
        Card(
          color: Colors.white70,
          margin: EdgeInsets.all(30.0),
          elevation: 8.0,
          child: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                txtEmail,
                SizedBox(height: 10,),
                txtPwd,
                SizedBox(height: 10,),
                btnLogin,
                SizedBox(height: 10,),
                checkSession,
              ],
            ),
          ),
        ),
        Positioned(
          child: logo,
          top: 200,
        ),
      ],
    );
  }
}