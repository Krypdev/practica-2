import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:practica_2/src/assets/configuration.dart';
import 'package:practica_2/src/database/database_helper.dart';
import 'package:practica_2/src/models/userdao.dart';

class Profile extends StatefulWidget {
  final UserDAO data;
  final String email;
  Profile(this.data, this.email, {Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  TextEditingController tcName = TextEditingController();
  TextEditingController tcLastPUser = TextEditingController();
  TextEditingController tcLastMUser = TextEditingController();
  TextEditingController tcEmail   = TextEditingController();
  TextEditingController tcPhone   = TextEditingController();

  final picker = ImagePicker();
  String imagePath = "";
  DatabaseHelper _database;
  Configuration conf = Configuration();

  @override
  void initState() { 
    super.initState();
    tcName.text = widget.data == null ? "" : widget.data.nameUser;
    tcLastPUser.text = widget.data == null ? "" : widget.data.lastPUser;
    tcLastMUser.text = widget.data == null ? "" : widget.data.lastMUser;
    tcEmail.text = widget.email;
    tcPhone.text = widget.data == null ? "" : widget.data.phone;
    imagePath = widget.data == null ? "" : widget.data.photo;
    _database = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {

    final imgPerfil = imagePath == ""?
      CircleAvatar(
        backgroundImage: NetworkImage('https://cdn.colombia.com/sdi/2020/01/28/cualidades-hacen-atractiva-mujer-805853.jpg'),
        radius: 80,
      )
      : CircleAvatar(
        radius: 80,
        child: ClipOval(
          child: Image.file(
            File(imagePath),
            width: 150,
            height: 150,
            fit: BoxFit.cover,
          ),
        )
      );

    final txtName = TextFormField(
      controller: tcName,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Introduce tu nombre',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtLastPUser = TextFormField(
      controller: tcLastPUser,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Introduce tu apellido paterno',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtLastMUser = TextFormField(
      controller: tcLastMUser,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Introduce tu apellido materno',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtEmail = TextFormField(
      controller: tcEmail,
      enabled: false,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: 'Introduce tu email',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtPhone = TextFormField(
      controller: tcPhone,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        hintText: 'Introduce tu telefono',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final button = RawMaterialButton(
      child: Icon(Icons.image_search),
      onPressed: () async {
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        imagePath = pickedFile != null ? pickedFile.path : "";
        setState(() {});
      },
      elevation: 2.0,
      fillColor: Configuration.colorItem,
      padding: EdgeInsets.all(10.0),
      shape: CircleBorder(),
    );

    final btnSave = RaisedButton(
      color: Colors.orangeAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text('Validar usuario', style: TextStyle(color: Colors.white),),
      onPressed: () async {
        final email = await conf.getEmail();
        print(email);
        final _objUser = await _database.getUser(email);

        if(_objUser == null){
          print("Insert");
          UserDAO user = UserDAO(
          nameUser  : tcName.text,
          lastPUser : tcLastPUser.text,
          lastMUser : tcLastMUser.text,
          phone     : tcPhone.text, 
          email     : tcEmail.text, 
          photo     : imagePath);

          conf.setEmail(user.email);

          _database.insert(user.toJSON(), 'tbl_perfil').then((data) => {
            print(data)
            //conf.setID(data.)
          });
        } else {
          print("Update");
          UserDAO user = UserDAO(
          id        : _objUser.id,
          nameUser  : tcName.text,
          lastPUser : tcLastPUser.text,
          lastMUser : tcLastMUser.text,
          phone     : tcPhone.text, 
          email     : tcEmail.text, 
          photo     : imagePath);

          conf.setEmail(user.email);

          _database.update(user.toJSON(), 'tbl_perfil').then((data) => {
            print(data)
          });
        }

        Navigator.pop(context);
        Navigator.pushNamed(context, '/dashboard');
      },
    );

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
          backgroundColor: Configuration.colorApp,
        ),
        body: Stack(
          children: <Widget>[
            Card(
              color: Colors.white10,
              margin: EdgeInsets.all(0.0),
              elevation: 0.0,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    imgPerfil,
                    SizedBox(height: 10,),
                    button,
                    SizedBox(height: 10,),
                    txtName,
                    SizedBox(height: 10,),
                    txtLastPUser,
                    SizedBox(height: 10,),
                    txtLastMUser,
                    SizedBox(height: 10,),
                    txtEmail,
                    SizedBox(height: 10,),
                    txtPhone,
                    SizedBox(height: 10,),
                    btnSave
                  ],
                ),
              ),
            ),
          ],
        )
      ),
    ); 
  }
}