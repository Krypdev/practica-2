import 'package:flutter/material.dart';
import 'package:practica_2/src/models/trending.dart';
import 'package:practica_2/src/network/api_trending.dart';
import 'package:practica_2/src/views/card_trending.dart';

class Trending extends StatefulWidget {
  Trending({Key key}) : super(key: key);

  @override
  _TrendingState createState() => _TrendingState();
}

class _TrendingState extends State<Trending> {

  ApiTrending apiTrending;
  @override
  void initState() { 
    super.initState();
    apiTrending = ApiTrending();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Trending'),
      ),
      body: FutureBuilder(
        future: apiTrending.getTrending(),
        builder: (BuildContext context, AsyncSnapshot<List<Result>> snapshot) {
          if(snapshot.hasError) {
            return Center(
              child: Text('Ocurrió un error'),
            );
          } else if(snapshot.connectionState == ConnectionState.done) {
            return _listTrending(snapshot.data);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _listTrending(List<Result> movies) {
    return ListView.builder(
      itemBuilder: (context, index) {
        Result trending = movies[index];
        return CardTrending(trending: trending,);
      }, 
      itemCount: movies.length,
    );
  }
}