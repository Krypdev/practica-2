import 'dart:io';

import 'package:flutter/material.dart';
import 'package:practica_2/src/assets/configuration.dart';
import 'package:practica_2/src/database/database_helper.dart';
import 'package:practica_2/src/models/userdao.dart';
import 'package:practica_2/src/screen/profile.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key}) : super(key: key);
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  Configuration conf = Configuration();
  DatabaseHelper _database;
  String email = "";

  @override
  void initState() {
    super.initState();
    _database = DatabaseHelper();
  }

  Future<UserDAO> getUserData() async{
    email = await conf.getEmail();
    return _database.getUser(email);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Peliculas'),
          backgroundColor: Configuration.colorApp,
        ),
        drawer: Drawer(
          child: FutureBuilder(
            future: getUserData(),
            builder: (BuildContext context, AsyncSnapshot<UserDAO> snapshot) {
              return
              ListView(
                children: <Widget>[
                  UserAccountsDrawerHeader(
                    decoration: BoxDecoration(
                      color: Configuration.colorApp,
                    ),
                    currentAccountPicture: (snapshot.data == null || snapshot.data.photo == "") ? 
                      CircleAvatar(
                        backgroundImage: NetworkImage('https://cdn.colombia.com/sdi/2020/01/28/cualidades-hacen-atractiva-mujer-805853.jpg'),
                      ) 
                      : ClipOval(
                        child: Image.file(File(snapshot.data.photo)),
                    ),
                    accountName: Text( snapshot.data == null ? "Name" : snapshot.data.nameUser+" "+snapshot.data.lastPUser+" "+snapshot.data.lastMUser),
                    accountEmail: Text( snapshot.data == null ? "Email" : snapshot.data.email),
                    onDetailsPressed: () async {
                      String email = await conf.getEmail();
                      UserDAO _objUser = await _database.getUser(email);

                      Navigator.pop(context);
                      Navigator.push(context,  MaterialPageRoute (
                        builder: (context) => Profile(_objUser, email)
                      ));
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.trending_up, color: Configuration.colorItem,),
                    title: Text('Trending'),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/trending');
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.search, color: Configuration.colorItem,),
                    title: Text('Search'),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/search');
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.favorite, color: Configuration.colorItem,),
                    title: Text('Favorites'),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/favorites');
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.exit_to_app, color: Configuration.colorItem,),
                    title: Text('Sing out'),
                    onTap: () async {
                      Configuration conf = Configuration();
                      conf.setActiveSession(false);

                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/login');

                      showDialog(
                        context: context,
                        builder: (BuildContext context){
                          return  AlertDialog(
                            title: Text('Cerrar sesión'),
                            content: Text('Adios'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text('close')
                                )
                            ],
                          );
                        } 
                      );

                    },
                  )
                ],
              );
            },
          )
        ),
      ),
    );
  }

}