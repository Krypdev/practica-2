import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:practica_2/src/models/trending.dart';

class ApiMovies {
  final String ENDPOINT = 'https://api.themoviedb.org/3/movie/popular?api_key=56937e86789a1dbb468fa42c88926b9d&language=en-US&page=1';
  Client http = Client();

  Future<List<Result>> getTrending() async {
    final response = await http.get(ENDPOINT);
    
    if (response.statusCode == 200){
      var movies = jsonDecode(response.body)['results'] as List;
      return movies.map((movie) => Result.fromJSON(movie)).toList();
    } else {
      return null;
    }
  }
}