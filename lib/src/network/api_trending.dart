import 'dart:convert';

import 'package:practica_2/src/models/trending.dart';
import 'package:practica_2/src/network/connection.dart';

class ApiTrending extends ConnectionApi {
  final String URL_TRENDING = "https://api.themoviedb.org/3/movie/popular?api_key=56937e86789a1dbb468fa42c88926b9d&language=en-US&page=1";

  Future<List<Result>> getTrending () async {
    final response = await this.get(URL_TRENDING, "");
    if( response != null ){
      var movies = jsonDecode( response )['results'] as List;
      List<Result> listMovies = movies.map((movie) => Result.fromJSON(movie)).toList();
      print(movies);
      return listMovies;
    }
    return null;
  }
}
