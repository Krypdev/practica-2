import 'dart:io';
import "package:http/http.dart" show Client;
import 'package:practica_2/src/assets/configuration.dart';

class ConnectionApi {
  String SERVER = "";
  
  Client http = Client();
  Configuration conf = Configuration();

  Future post(url, data) async {
    String token = await conf.getToken();
    
    print(data.toJSON());
    final response = await http.post(
      SERVER+url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
      body: data.toJSON()
    );
    if( response.statusCode == 200 ) return response.body;
    else return null;
  }

  Future get(url, params) async {
    String token = await conf.getToken();

    final response = await http.get(
      SERVER+url+params,
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );
    if( response.statusCode == 200 ) return response.body;
    else return null;
  }
}
